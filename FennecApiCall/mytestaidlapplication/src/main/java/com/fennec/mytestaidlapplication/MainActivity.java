package com.fennec.mytestaidlapplication;

import android.Manifest;
import android.animation.ArgbEvaluator;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileObserver;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Base64InputStream;
import android.util.Base64OutputStream;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.internal.app.IMediaContainerService;
import com.fennec.apicallexample.IFennecApiCall;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends AppCompatActivity {
    private final static String TAG = "FennecTestAidl";
    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.title_home);
                    return true;
                case R.id.navigation_dashboard:
                    mTextMessage.setText(R.string.title_dashboard);
                    return true;
                case R.id.navigation_notifications:
                    mTextMessage.setText(R.string.title_notifications);
                    return true;
            }
            return false;
        }
    };

    @BindView(R.id.test_aidl)
    Button testButton;

//    private static final String STR_REMOTE_PACKAGE_NAME = "com.android.defcontainer";
//    private static final String STR_REMOTE_SERVICE_NAME = STR_REMOTE_PACKAGE_NAME + ".DefaultContainerService";

    private static final String STR_REMOTE_PACKAGE_NAME = "com.fennec.apicallexample";
    private static final String STR_REMOTE_SERVICE_NAME = STR_REMOTE_PACKAGE_NAME + ".DispatchingService";

    @OnClick(R.id.test_aidl)
    public void remoteServiceConnection() {
        if (!mIsServiceBound) {
            ComponentName componentName = new ComponentName(
                    STR_REMOTE_PACKAGE_NAME,
                    STR_REMOTE_SERVICE_NAME);
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setComponent(componentName);
            bindService(intent, mServiceConnection, BIND_AUTO_CREATE);
        } else {
            try {
                Task<Location> task = mFusedLocationClient.getLastLocation();
                mFusedLocationClient.requestLocationUpdates(new LocationRequest(),
                        new LocationCallback(){
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        Log.d(TAG, "onLocationResult");

                    }
                }, null);
                task.addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        Log.d(TAG, "onSuccess++");
                        if (location != null) {
                            Log.d(TAG, "location" + location);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "onFailure++");
                        e.printStackTrace();
                    }
                });

            } catch (Exception e) {
                Log.d(TAG, "getFirebaseToken " + e);
                e.printStackTrace();
            }
//            Log.d(TAG, "getFirebaseToken " + s);
        }
    }

    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;

        ButterKnife.bind(this);
        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        String path1 = "/mnt/sdcard/threewatch_folder/LimitPlaySetting.dat";

        FileObserver fileObserver = new FileObserver("") {
            @Override
            public void onEvent(int event, @Nullable String path) {
                switch (event) {
                    case FileObserver.MODIFY:
                        Log.d(TAG, "MODIFY: update the array 1");
                        readFile(path);
                        break;
                    case FileObserver.CLOSE_WRITE:
                        Log.d(TAG, "CLOSE_WRITE: update the array 2");
                        readFile(path);
                        break;
                    default:
                        Log.d(TAG, "CLOSE_WRITE: update the array other " + event);

                }
            }
        };
        fileObserver.startWatching();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

    }

    private FusedLocationProviderClient mFusedLocationClient;

    String fakeAccount = "alan5196@gmail.com";
    String fakeAccountToken =
            "RO3t9Bfe0rMh9pa3r/wg9l821vjB/Y54WCpIAq+e4IEsGM0aKJ8sILHEOdZZoSvgPkxtuYRBQs9l+MYcqhnJaTn2LOTVRumKCHMrDgB6bhFNuCbBsJt3aPZNFbxfvbKe5WchY/Q1jRskLylmRZUVxBVOfyZJfB4gSJ35aMp93Ys=";


    private String getFirebaseToken() {
        try {
            String token = mService.getDeviceToken();
            return token;
        } catch (RemoteException e) {
            Log.d(TAG, "getToken failed.");
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void updateArray(byte[] arr, int at, String value) {
        ArrayList<String> limitArrays = new ArrayList<>(410);

        try {
            FileInputStream fileInputStream = new FileInputStream(this.getFilesDir() + "/sample.txt");
            Base64InputStream inputStream = new Base64InputStream(fileInputStream, Base64.DEFAULT);
            int byteRead = -1;

            StringBuilder stringBuilder = new StringBuilder();
            while ((byteRead = inputStream.read()) != -1) {
                if (byteRead == 44) {
                    limitArrays.add(stringBuilder.toString());
                    stringBuilder.setLength(0);
                    continue;
                }

                stringBuilder.append(Character.toString((char) byteRead));
//                limitArrays.add(Character.toString((char)byteRead));
            }
            limitArrays.add(stringBuilder.toString());
            fileInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        limitArrays.set(at, value);
        StringBuilder stringBuilder = new StringBuilder();
        for (String s : limitArrays) {
            stringBuilder.append(s).append(',');
        }

        stringBuilder.setLength(stringBuilder.length() - 1);
        String needProcessingToByte = stringBuilder.toString();

        File f;
        FileOutputStream fos = null;
        try {
            f = new File(this.getFilesDir() + "/", "sample.txt");
            fos = new FileOutputStream(f);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            OutputStream base64OutputStream = new Base64OutputStream(fos, Base64.DEFAULT);
            for (byte b : needProcessingToByte.getBytes())
                base64OutputStream.write(b);
            base64OutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    void stackoverflow(int at, char[] chars) {
        final int PREV_COMMA = at - 1;
        final int END_COMMA = at;
        int prev = 0, end = at;

        try {
            Base64InputStream base64InputStream = new Base64InputStream(new FileInputStream("123.txt"), Base64.DEFAULT);
            base64InputStream.read();
            byte[] buffer = new byte[8192];
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);
            try {
                while ((bytesRead = base64InputStream.read()) != -1) {
                    if (bytesRead == 44) {
                        prev++;
                        if (prev == PREV_COMMA) {
                            byte[] bytes = new byte[chars.length];
                            int k = 0;
                            for (char ch : chars) {
                                bytes[k] = Byte.parseByte(String.valueOf(ch));
                            }

                            output64.write(bytes);

//                            skipUntilNextComma();
                        }
                    }

                    while ((bytesRead = base64InputStream.read(buffer)) != -1) {
                        output64.write(buffer, 0, bytesRead);
                    }

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            output64.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

//        attachedFile = output.toString();

    }

    private String readFile(String path) {
        try {
//            BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
//            String needConvert = bufferedReader.readLine();
//            String needConvert = "MSwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCwwLDAsMCww";
//            byte[] bytes = Base64.decode(needConvert, Base64.DEFAULT);
            updateArray(null, 371, "1830");
//            String encodeString = Base64.encodeToString(new Byte[]{} , Base64.DEFAULT);
            return "";
        } catch (Exception localException) {
            localException.printStackTrace();
        }
        return ";";
    }

    private void writeFile(String path, String writeString) {
        try {
            File file = new File(path);
            if (file.exists() && file.canWrite()) {
                FileWriter fileWriter = new FileWriter(file);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                bufferedWriter.write(writeString);
                bufferedWriter.flush();
                bufferedWriter.close();
                fileWriter.flush();
                fileWriter.close();
            }

        } catch (Exception localException) {
            localException.printStackTrace();
        }
    }


    boolean mIsServiceBound = false;
    //    IMediaContainerService mService;
    IFennecApiCall mService;
    ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, "onServiceConnected");
            mIsServiceBound = true;
            mService = IFennecApiCall.Stub.asInterface(iBinder);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(TAG, "onServiceDisconnected");
            mIsServiceBound = false;
            mService = null;
        }
    };

    final static String primaryVolume = "/storage/emulated/0";
    final static String systemPath = "/system/";
    final static String CATEGORY_MUSIC = "music";
    final static String CATEGORY_PHOTO_VIDEO = "video_pic";
    final static String CATEGORY_SYSTEM = "system";
    final static String CATEGORY_AVAILABLE = "available";
    final static String CATEGORY_TOTAL = "total";

    private HashMap<String, Long> measureExactStorage(IMediaContainerService imcs) {
        final Context context = mContext;
        if (context == null) {
            return null;
        }


        HashMap<String, HashMap<String, Long>> categories = new HashMap<>();
        HashMap<String, Long> musicMap = new HashMap<>();

        musicMap.put(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).toString(), 0L);
        musicMap.put(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_ALARMS).toString(), 0L);
        musicMap.put(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_NOTIFICATIONS).toString(), 0L);
        musicMap.put(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_RINGTONES).toString(), 0L);
        musicMap.put(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PODCASTS).toString(), 0L);

        HashMap<String, Long> videoPhotoMap = new HashMap<>();
        videoPhotoMap.put(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).toString(), 0L);
        videoPhotoMap.put(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).toString(), 0L);

        HashMap<String, Long> systemMap = new HashMap<>();
        systemMap.put(systemPath, 0L);

        categories.put(CATEGORY_MUSIC, musicMap);
        categories.put(CATEGORY_PHOTO_VIDEO, videoPhotoMap);
        categories.put(CATEGORY_SYSTEM, systemMap);

        HashMap<String, Long> resultMap = new HashMap<>();
        long volumeAvailable = -1, volumeTotal = -1;
        try {
            long[] sizeArray = imcs.getFileSystemStats(primaryVolume);
            volumeTotal = sizeArray[0];
            volumeAvailable = sizeArray[1];
            Log.d(TAG, "volumeTotal:" + volumeTotal + " volumeAvailable " + volumeAvailable);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //resultMap.put(CATEGORY_TOTAL, volumeTotal); No need to use this value
        resultMap.put(CATEGORY_AVAILABLE, volumeAvailable);

        for (String typeOfCategories : categories.keySet()) {
            Log.d(TAG, "Type: " + typeOfCategories + " processing");

            HashMap<String, Long> category = categories.get(typeOfCategories);
            long categorySize = 0;
            for (String path : category.keySet()) {
                try {
                    final long size = imcs.calculateDirectorySize(path);
                    Log.d(TAG, "measureExactStorage path" + path + " size:" + size);
                    category.put(path, size);
                    categorySize += size;
                } catch (RemoteException e) {
                    e.printStackTrace();
                } finally {
                    Log.d(TAG, "measureExactStorage finally" + categorySize);
                    resultMap.put(typeOfCategories, categorySize);
                }
            }
        }

        for (String k : resultMap.keySet())
            Log.d(TAG, "result k " + k + " value " + resultMap.get(k));

        return resultMap;

    }


}
