// IFennecApiCall.aidl
package com.fennec.apicallexample;

// Declare any non-default types here with import statements

interface IFennecApiCall {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    String getDeviceToken();
    void setChildId(in String childId);
    String getChildId();
    Map getCtrlPermissions();
    Map getBanTimeMap();
    boolean updateBanTimeGame(in int index,in String what);
}
