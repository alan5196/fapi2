// IMediaContainerService.aidl
package com.android.internal.app;

interface IMediaContainerService {
    long calculateDirectorySize(String directory);
    long[] getFileSystemStats(String path);
}

