package com.fennec.apicallexample;

import android.os.AsyncTask;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.ANResponse;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NetworkPostUtil extends NetworkUtil{
    private JSONObject mQueryObject;
    private Map<String, String> mHeaders;

    NetworkPostUtil(Map<String, String> headers, JSONObject queryParameters, String... apis){
        mQueryURL = buildRequestURL(apis);

        for (String k:headers.keySet()){
            Log.d(TAG, headers.get(k));
        }

        mQueryObject = queryParameters;
        mHeaders = headers;
    }

    final TaskCompletionSource<Boolean> taskCompletionSource = new TaskCompletionSource<Boolean>();


    class MyAsyncTask extends AsyncTask<TaskCompletionSource<Boolean>, Void, TaskCompletionSource<Boolean>>{
        TaskCompletionSource<Boolean> mResult;
        @Override
        protected TaskCompletionSource<Boolean> doInBackground(TaskCompletionSource<Boolean>... taskCompletionSources) {

            if (taskCompletionSources == null){
                return null;
            }

            mResult = taskCompletionSources[0];
            Log.d(TAG, "MyAsyncTask doInBackground");

            ANRequest request = AndroidNetworking.post(mQueryURL)
                    .addJSONObjectBody(mQueryObject)
                    .addHeaders(mHeaders)
                    .build();

            ANResponse<JSONObject> response = request.executeForJSONObject();

            if (response.isSuccess()){
                try {
                    JSONObject jsonObject = response.getResult();
                    Log.d(TAG, "MyAsyncTask doInBackground" + jsonObject.toString());
                    mResult.setResult(jsonObject.getBoolean("success"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    mResult.setException(e);
                }
            } else {
                mResult.setException(new Exception("Not ok response is not success"));
            }

            return mResult;
        }

    }

    public Task<Boolean> startWork(){
        MyAsyncTask task = new MyAsyncTask();
        task.execute(taskCompletionSource);
        return taskCompletionSource.getTask();
    }
}
