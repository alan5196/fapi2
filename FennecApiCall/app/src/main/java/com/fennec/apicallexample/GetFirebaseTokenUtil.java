package com.fennec.apicallexample;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

class GetFirebaseTokenUtil {
    private String TAG = this.getClass().getSimpleName();
    String mToken = null;
    Context mContext;
    GetFirebaseTokenUtil(Context context){
        mContext = context;
    }
    private final int FIREBASE_TOKEN_TIME_OUT = 15000;
    public String getToken() throws Exception {

        checkGoogle();

        Task<InstanceIdResult> result = FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCanceledListener(mTokenAskingCancelListener)
                .addOnFailureListener(mTokenFailListener)
                .addOnCompleteListener(mTokenAskingCompleteListener)
                .addOnSuccessListener(mTokenAskingSuccessListener);
        // Trick:: Listener checking IN ORDER.
        // Complete then return ensure no exception happen.
        synchronized (this) {
            while (!result.isComplete()) {
                Log.d(TAG, "getToken wait until notified token:" + mToken);
                this.wait(FIREBASE_TOKEN_TIME_OUT);
            }
        }

        Log.d(TAG, "getToken token:" + mToken);

        return mToken;
    }

    private void checkGoogle() {
        int googlePlayServicesAvailableStatus = GoogleApiAvailability.getInstance().
                isGooglePlayServicesAvailable(mContext);
        Log.d(TAG, "googlePlayServicesAvailableStatus" + googlePlayServicesAvailableStatus);
    }

    private final OnCompleteListener<InstanceIdResult> mTokenAskingCompleteListener =
            new OnCompleteListener<InstanceIdResult>() {
                @Override
                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                    checkGoogle();
                    try {
                        if (task.isSuccessful()){
                            Log.d(TAG, "task.isSuccessful()");
                            mToken = task.getResult().getToken();
                        }
                        else {
                            if (task.getException() != null){
                                Log.d(TAG, "task failed getException: "
                                        + task.getException().getMessage());
                                task.getException().printStackTrace();
                            } else {
                                Log.d(TAG, "task failed getException: null ");
                            }
                            mToken = null;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (mToken == null){
                        mToken = FirebaseInstanceId.getInstance().getToken();
                        for (int i = 0; i < 5; i++) {
                            try {
                                Thread.sleep(1000);
                                mToken = FirebaseInstanceId.getInstance().getToken();
                                Log.d(TAG, "onComplete deprecated retry" + i + '\t' + mToken);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            if (!TextUtils.isEmpty(mToken)) {
                                Log.d(TAG, "got token");
                                break;
                            }
                        }
                    }

                    synchronized (GetFirebaseTokenUtil.this) {
                        Log.e(TAG, "onComplete notify " + mToken);
                        GetFirebaseTokenUtil.this.notify();
                    }
                }
            };

    private final OnFailureListener mTokenFailListener = new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
            checkGoogle();
            Log.e(TAG, "onFailure reason:" + e.getCause());
            e.printStackTrace();
        }
    };

    private final OnSuccessListener<InstanceIdResult> mTokenAskingSuccessListener =
            new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    Log.i(TAG, "onSuccess got token " + instanceIdResult.getToken());
                }
            };

    private final OnCanceledListener mTokenAskingCancelListener = new OnCanceledListener() {
        @Override
        public void onCanceled() {
            Log.d(TAG, "onCanceled mTokenAskingCancelListener.");
        }
    };
}
