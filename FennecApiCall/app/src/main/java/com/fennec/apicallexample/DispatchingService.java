package com.fennec.apicallexample;

import android.app.Service;
import android.content.ComponentName; 
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Base64InputStream;
import android.util.Base64OutputStream;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.android.internal.app.IMediaContainerService;
import static android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED;


/**
 * This class is alive from boot-completed.
 * And will listening to getDeviceToken from the backend
 */
public class DispatchingService extends Service {
    private String TAG = this.getClass().getSimpleName();
    private String mChildId = null;
    private Context mContext;
    private TelephonyManager mTelephony;
    private final static int MSG_CLOSE_GPS = 0;
    private final static int MSG_GET_FUSED_LOCATION = 1;
    private final static int MSG_ENABLE_GPS = 2;
    private final static int MSG_UPLOAD_LOCATION = 3;
    private final static int MSG_NOTIFY_DB_LOCATION_UPDATED = 4;

    private final static int MSG_BIND_MEDIA_CONTAINER_SERVICE = 5;
    private final static int MSG_BOUND_MEDIA_CONTAINER_SERVICE = 6;
    private final static int MSG_UPLOAD_STORAGE = 7;
    private final static int MSG_NOTIFY_DB_STORAGE_UPDATED = 8;

    private final static int MSG_GET_BATTERY_STAT = 9;
    private final static int MSG_UPLOAD_BATTERY = 10;
    private final static int MSG_NOTIFY_DB_BATTERY_UPDATED = 11;
    private final static int MSG_LISTEN_REALTIME_MAPS = 12;
    private final static String VERSION = "b";

    private final static int MSG_ERROR = 1000;

    private String intToString(int i){
        switch (i){
            case MSG_CLOSE_GPS:
                return "MSG_CLOSE_GPS";
            case MSG_ENABLE_GPS:
                return "MSG_ENABLE_GPS";
            case MSG_UPLOAD_LOCATION :
                return "MSG_UPLOAD_LOCATION";
            case MSG_NOTIFY_DB_LOCATION_UPDATED:
                return "MSG_NOTIFY_DB_LOCATION_UPDATED";
            case MSG_BIND_MEDIA_CONTAINER_SERVICE:
                return "MSG_BIND_MEDIA_CONTAINER_SERVICE";
            case MSG_BOUND_MEDIA_CONTAINER_SERVICE:
                return "MSG_BOUND_MEDIA_CONTAINER_SERVICE";
            case MSG_UPLOAD_STORAGE:
                return "MSG_UPLOAD_STORAGE";
            case MSG_NOTIFY_DB_STORAGE_UPDATED:
                return "MSG_NOTIFY_DB_STORAGE_UPDATED";
            case MSG_GET_BATTERY_STAT:
                return "MSG_GET_BATTERY_STAT";
            case MSG_UPLOAD_BATTERY:
                return "MSG_UPLOAD_BATTERY";
            case MSG_NOTIFY_DB_BATTERY_UPDATED:
                return "MSG_NOTIFY_DB_BATTERY_UPDATED";
            case MSG_LISTEN_REALTIME_MAPS:
                return "MSG_LISTEN_REALTIME_MAPS";
            case MSG_GET_FUSED_LOCATION:
                return "MSG_GET_FUSED_LOCATION";
            case MSG_ERROR:
                return "MSG_ERROR";
        }
        return Integer.toString(i);
    }

    private final static int CLOSE_GPS_TIMEOUT = 15 * 60 * 1000; // 15 minutes
    private Handler mHandler;
    private HandlerThread mHandlerThread;
    private static String sDeviceId;
    private static String PREF_NAME = "fennec_service_shrd_pref";
    private IMediaContainerService mediaContainerService = null;
    private static final String STR_REMOTE_PACKAGE_NAME = "com.android.defcontainer";
    private static final String STR_REMOTE_SERVICE_NAME = STR_REMOTE_PACKAGE_NAME
            + ".DefaultContainerService";
    private boolean mIsServiceBound = false;
    ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, "onServiceConnected");
            mediaContainerService = IMediaContainerService.Stub.asInterface(iBinder);
            mHandler.sendMessage(mHandler.obtainMessage(MSG_BOUND_MEDIA_CONTAINER_SERVICE));
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(TAG, "onServiceDisconnected");
            mIsServiceBound = false;
            mediaContainerService = null;
        }
    };

    private Task<Void> updateWatchInfoPower(int value){
        Map<String, Object> map = new HashMap<>(1);
        map.put("power", value);

        return FirebaseDatabase.getInstance().getReference("watch-settings").child(mChildId)
                .child("watchInfos/" + sDeviceId + "/").updateChildren(map)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "updateWatchInfoPower onSuccess");
                        mHandler.sendMessage(mHandler.obtainMessage(MSG_NOTIFY_DB_BATTERY_UPDATED));
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "updateWatchInfoPower onFailure retry");
                    }
                });
    }

    private Task<Void> updateWatchInfoSpace(Bundle stat){
        DatabaseReference spaceRef = FirebaseDatabase.getInstance().getReference("watch-settings")
                .child(mChildId).child("watchInfos/" + sDeviceId + "/space");

        Map<String, Object> map = new HashMap<>(4);
        map.put("system", getKBs(stat.getLong(CATEGORY_SYSTEM)));
        map.put("photoVideo", getKBs(stat.getLong(CATEGORY_PHOTO_VIDEO)));
        map.put("music", getKBs(stat.getLong(CATEGORY_MUSIC)));
        map.put("available", getKBs(stat.getLong(CATEGORY_AVAILABLE)));


        return spaceRef.updateChildren(map).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "updateWatchInfoSpace onSuccess");
                mHandler.sendMessage(mHandler.obtainMessage(MSG_NOTIFY_DB_STORAGE_UPDATED));
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "updateWatchInfoSpace onFailure");
            }
        });
    }

    private Task<Void> updateWatchInfoLocation(Location location){
        Map<String, Object> map = new HashMap<>(1);
        map.put("latitude", location.getLatitude());
        map.put("longitude", location.getLongitude());

        return FirebaseDatabase.getInstance().getReference("watch-settings").child(mChildId)
                .child("watchInfos/" + sDeviceId + "/location").updateChildren(map)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "updateWatchInfoLocation onSuccess");
                        mHandler.sendMessage(mHandler.obtainMessage(MSG_NOTIFY_DB_LOCATION_UPDATED));
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "updateWatchInfoLocation onFailure retry");
                    }
                });
    }
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LocationCallback mLocationCallback = new LocationCallback(){
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Log.d(TAG, "onLocationResult");
            if(locationResult.getLastLocation() != null){
                Message msg = mHandler.obtainMessage(MSG_UPLOAD_LOCATION);
                msg.obj = locationResult.getLastLocation();
                mHandler.sendMessage(msg);
            }
        }
    };

    LocationRequest mLocationRequest;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        mTelephony = (TelephonyManager) mContext.getSystemService(TELEPHONY_SERVICE);
        sDeviceId = mTelephony.getDeviceId();

        //TODO: Setting valid child id.
        mChildId = getSharedPreferences(PREF_NAME, MODE_PRIVATE).getString(KEY_CHILD_ID,
                "");


        Log.d(TAG, "onCreate loading child id " + mChildId + "version" + VERSION);
        mHandlerThread = new HandlerThread("ServiceHandler");
        mHandlerThread.start();
        mHandler = new Handler(mHandlerThread.getLooper(), new Handler.Callback() {
            int locationRetry = 0;
            final int RETRY_LIMIT = 2;
            @Override
            public boolean handleMessage(Message msg) {
                Log.d(TAG, "meessage got " + intToString(msg.what));
                switch (msg.what){
                    case MSG_GET_FUSED_LOCATION:{
                        createLocationRequest();
                        mFusedLocationProviderClient = new FusedLocationProviderClient(mContext);
                        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, null);
                        break;
                    }
                    case MSG_CLOSE_GPS:
                        handleCloseGps();
                        break;
                    case MSG_ENABLE_GPS:
                        resettingGpsTimeout();
                        enableGpsIfNeeded();
                        break;
                    case MSG_UPLOAD_LOCATION:{
                        Location location = (Location) msg.obj;
                        updateWatchInfoLocation(location);
                        break;
                    }
                    case MSG_NOTIFY_DB_LOCATION_UPDATED:
                        clearRemoteDatabaseValue("updateLocation", false);
                        mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
                        break;
                    case MSG_BIND_MEDIA_CONTAINER_SERVICE:
                        if (!mIsServiceBound) {
                            ComponentName componentName = new ComponentName(
                                    STR_REMOTE_PACKAGE_NAME,
                                    STR_REMOTE_SERVICE_NAME);
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.setComponent(componentName);
                            bindService(intent, mServiceConnection, BIND_AUTO_CREATE);
                            mIsServiceBound = true;
                        } else {
                            mHandler.sendMessage(mHandler.obtainMessage(MSG_BOUND_MEDIA_CONTAINER_SERVICE));
                        }
                        break;
                    case MSG_BOUND_MEDIA_CONTAINER_SERVICE:
                        if (mIsServiceBound){
                            Message message = mHandler.obtainMessage(MSG_UPLOAD_STORAGE);
                            message.setData(measureExactStorage(mediaContainerService));
                            mHandler.sendMessage(message);
                        } else{
                            mHandler.sendMessage(mHandler.obtainMessage(
                                    MSG_BIND_MEDIA_CONTAINER_SERVICE));
                            Log.d(TAG, "The service is not bound. Rebind");
                        }
                        break;
                    case MSG_UPLOAD_STORAGE:{
                        updateWatchInfoSpace(msg.getData());
                        break;
                    }
                    case MSG_NOTIFY_DB_STORAGE_UPDATED:
                        clearRemoteDatabaseValue("updateSpace", false);
                        unbindServiceDelayed();
                        break;

                    case MSG_GET_BATTERY_STAT:{
                        BatteryManager batteryManager = (BatteryManager)
                                mContext.getSystemService(BATTERY_SERVICE);
                        if (batteryManager != null){
                            Message message = mHandler.obtainMessage(MSG_UPLOAD_BATTERY);
                            message.arg1 = batteryManager.getIntProperty(
                                    BatteryManager.BATTERY_PROPERTY_CAPACITY);
                            mHandler.sendMessage(message);
                        }
                        else{
                            Log.e(TAG, "Battery Manager null.");
                        }
                        break;
                    }
                    case MSG_UPLOAD_BATTERY:{
                        final int batteryLevel = msg.arg1;
                        Log.d(TAG, "Battery Level: " + batteryLevel);
                        updateWatchInfoPower(batteryLevel);
                        break;
                    }
                    case MSG_NOTIFY_DB_BATTERY_UPDATED:{
                        clearRemoteDatabaseValue("updatePower", false);
                        break;
                    }
                    case MSG_LISTEN_REALTIME_MAPS:
                        listenCtrlMap();
                        listenCtrlPermissionMap();
                        listenBanTime();
                        listenGameTime();
                        break;
                    case MSG_ERROR:
                        String errorMessage =(String) msg.obj;
                        Log.d(TAG, "Error from " + errorMessage);
                        break;
                }
                return true;
            }

        });

        if (!TextUtils.isEmpty(mChildId)){
            mHandler.sendEmptyMessage(MSG_LISTEN_REALTIME_MAPS);
        } else {
            Log.d(TAG, "mChildId is empty in sharedPreference maybe the watch was'nt bound successfully");
        }

    }

    private void listenCtrlPermissionMap() {
        Log.d(TAG, "listenCtrlPermissionMap");
        FirebaseDatabase.getInstance().getReference("watch-settings").child(mChildId).child("ctrlPermission")
            .addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Log.d(TAG, "onDataChange CtrlPermissionMap");
                    if (dataSnapshot.getValue(true) == null){
                        Log.d(TAG, "ctrlPermission was null");
                        return;
                    }

                    mCtrlPermission.clear();
                    mCtrlPermission.putAll((Map<String, Object>) dataSnapshot.getValue(true));
                    updatePublicCtrlPermissionMap(mCtrlPermission);
                    updateReservedCtrlPermissionMap(mCtrlPermission);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.d(TAG, "watch-settings ctrlPermission cancel please check.");
                }
            });
    }

    Map<String, Object> mGameTimeMap = new HashMap<>();
    private void listenGameTime() {
        Log.d(TAG, "listenGameTime");
        FirebaseDatabase.getInstance().getReference("watch-settings").child(mChildId).child("gameTime")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if (dataSnapshot.getValue(true) == null){
                            Log.d(TAG, "listenGameTime was null");
                            return;
                        }

                        mGameTimeMap.clear();
                        mGameTimeMap.putAll((Map<String, Object>) dataSnapshot.getValue(true));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d(TAG, "watch-settings listenGameTime cancel please check.");
                    }
                });
    }

    Map<String, Object> mBanTimeMap = new HashMap<>();
    private void listenBanTime() {
        Log.d(TAG, "listenBanTime");
        FirebaseDatabase.getInstance().getReference("watch-settings").child(mChildId).child("banTime")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if (dataSnapshot.getValue(true) == null){
                            Log.d(TAG, "listenBanTime was null");
                            return;
                        }

                        mBanTimeMap.clear();
                        mBanTimeMap.putAll((Map<String, Object>) dataSnapshot.getValue(true));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d(TAG, "watch-settings listenBanTime cancel please check.");
                    }
                });
    }



    private void updateArray(int at, String value){
        String path = "/mnt/sdcard/threewatch_folder/LimitPlaySetting.dat";
        ArrayList<String> limitArrays = new ArrayList<>(410);
        try {
            FileInputStream fileInputStream = new FileInputStream(path);
            Base64InputStream inputStream = new Base64InputStream(fileInputStream, Base64.DEFAULT);
            int byteRead = -1;

            StringBuilder stringBuilder = new StringBuilder();
            while((byteRead = inputStream.read()) != -1){
                if (byteRead == 44){
                    limitArrays.add(stringBuilder.toString());
                    stringBuilder.setLength(0);
                    continue;
                }

                stringBuilder.append(Character.toString((char)byteRead));
            }
            limitArrays.add(stringBuilder.toString());
            fileInputStream.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        limitArrays.set(at, value);
        StringBuilder stringBuilder = new StringBuilder();
        for (String s : limitArrays){
            stringBuilder.append(s).append(',');
        }

        stringBuilder.setLength(stringBuilder.length()-1);
        String needProcessingToByte = stringBuilder.toString();

        File f;
        FileOutputStream fos = null;
        try{
            f = new File(path);
            fos = new FileOutputStream(f);
        }catch (Exception e){
            e.printStackTrace();
        }

        try{
            OutputStream base64OutputStream = new Base64OutputStream(fos, Base64.DEFAULT);
            for (byte b : needProcessingToByte.getBytes())
                base64OutputStream.write(b);
            base64OutputStream.close();
            fos.close();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void listenCtrlMap() {
        Log.d(TAG, "listenCtrlMap");
        FirebaseDatabase.getInstance().getReference("watch-settings").child(mChildId).child("ctrl")
            .addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.d(TAG, "watch_settingsListener onDataChange");
                    Map<String, Object> ctrlMap = (Map<String, Object>) dataSnapshot.getValue(true);
                    if (ctrlMap != null){
                        backupCtrlMap(ctrlMap);
                        if ((boolean)ctrlMap.get("updateLocation")){
                            mHandler.sendMessage(mHandler.obtainMessage(MSG_ENABLE_GPS));
                            mHandler.sendMessage(mHandler.obtainMessage(MSG_GET_FUSED_LOCATION));
                        } else if ((boolean)ctrlMap.get("updateSpace")){
                            Log.d(TAG, "updateSpace true");
                            mHandler.sendMessage(mHandler.obtainMessage(MSG_BIND_MEDIA_CONTAINER_SERVICE));
                        } else if ((boolean)ctrlMap.get("updatePower")){
                            Log.d(TAG, "updatePower true");
                            mHandler.sendMessage(mHandler.obtainMessage(MSG_GET_BATTERY_STAT));
                        }else {
                            Log.d(TAG, "No need to change anything for now");
                        }
                    } else{
                        Log.w(TAG, "ctrl map was null please check it out");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.d(TAG, "watch-settings cancel please check.");
                }
            });
    }

    final Map<String, Object> mCtrlPermission = new HashMap<>();
    final Map<String, Object> mPublicCtrlPermission = new HashMap<>();
    final Map<String, Object> mReservedCtrlPermission = new HashMap<>();

    /** Requirement here.
    https://docs.google.com/spreadsheets/d/1i99r_lvhoYKeZI9YNR2UzwiA5Ivm4W-YY0XmpFHZYNc/edit#gid=0
     */

    void updatePublicCtrlPermissionMap(Map map){
        mPublicCtrlPermission.clear();
        mPublicCtrlPermission.put("allowToModifyPhoneBook", map.get("allowToModifyPhoneBook"));

        mPublicCtrlPermission.put("incomingCallRestriction", map.get("incomingCallRestriction"));
        mPublicCtrlPermission.put("outgoingCallRestriction", map.get("outgoingCallRestriction"));
        updateCallRestrictionPersist(map);
        updateAllowToModifyPhoneBookMapPersist(map);

        mPublicCtrlPermission.put("smsRestrictions", map.get("smsRestrictions"));
        mPublicCtrlPermission.put("forwardAllSMS", map.get("forwardAllSMS"));
        mPublicCtrlPermission.put("videoCall", map.get("videoCall"));
        mPublicCtrlPermission.put("use3G", map.get("use3G"));
    }

    private void updateCallRestrictionPersist(Map map) {
        String incomingCallRestriction = map.get("incomingCallRestriction").equals(true) ? "1" : "0";
        String outgoingCallRestriction = map.get("outgoingCallRestriction").equals(true) ? "1" : "0";
        Log.d(TAG, "updateCallRestrictionPersist In: " + incomingCallRestriction +
                " Out: " + outgoingCallRestriction);
        SystemPropertiesProxy.set("persist.incall.whitelist", incomingCallRestriction);
        SystemPropertiesProxy.set("persist.outcall.whitelist", outgoingCallRestriction);
    }

    private void updateAllowToModifyPhoneBookMapPersist(Map map) {
        String allowToModifyPhoneBook = map.get("allowToModifyPhoneBook").equals(true) ? "1" : "0";
        Log.d(TAG, "updateAllowToModifyPhoneBookMapPersist allowToModifyPhoneBook:" + allowToModifyPhoneBook);
        SystemPropertiesProxy.set("persist.modify.phonebook", allowToModifyPhoneBook);
    }

    void updateReservedCtrlPermissionMap(Map map){
        mReservedCtrlPermission.clear();
        mReservedCtrlPermission.put("powerSaveGame", map.get("powerSaveGame"));
        mReservedCtrlPermission.put("powerSaveMain", map.get("powerSaveMain"));
        mReservedCtrlPermission.put("disturb", map.get("disturb"));
        mReservedCtrlPermission.put("voiceRoaming", map.get("voiceRoaming"));
        mReservedCtrlPermission.put("dataRoaming", map.get("dataRoaming"));
        mReservedCtrlPermission.put("sleepMode", map.get("sleepMode"));
    }

    private void unbindServiceDelayed() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "preparedToDisconnectService " + mIsServiceBound);
                if (mIsServiceBound){
                    unbindService(mServiceConnection);
                    mIsServiceBound = false;
                }
            }
        }, 60 * 1000);
    }

    Map<String, Object> mCtrlMap = null;
    void backupCtrlMap(Map<String, Object> map){
        mCtrlMap = map;
    }
    Map<String, Object> restoreCtrlMap(){
        return mCtrlMap;
    }

    void clearRemoteDatabaseValue(String key, boolean val){
        DatabaseReference databaseReference = FirebaseDatabase.getInstance()
                .getReference("watch-settings").child(mChildId).child("ctrl");

        Map<String, Object> map = restoreCtrlMap();
        map.put(key, val);

        databaseReference
            .updateChildren(map, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError,
                                       @NonNull DatabaseReference databaseReference) {
                    Log.d(TAG, "databaseReference onComplete");
                    if (databaseError != null){
                        Message msg = mHandler.obtainMessage(MSG_ERROR);
                        msg.obj = databaseError.getDetails();
                        mHandler.sendMessage(msg);
                        return;
                    }
                }
            });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mContext = null;
        mHandler.removeCallbacksAndMessages(null);
        mHandlerThread.quit();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onbind version " + VERSION);
        return mBinder;
    }

    private static final String KEY_CHILD_ID = "child_id";
    private final IFennecApiCall.Stub mBinder = new IFennecApiCall.Stub() {
        public String getDeviceToken() {
            GetFirebaseTokenUtil util = new GetFirebaseTokenUtil(mContext);
            try {
                return util.getToken();
            } catch (Exception e) {
                Log.d(TAG, "getDeviceToken exception catched");
                e.printStackTrace();
                return util.mToken;
            }
        }
        public void setChildId(String childId){
            Log.d(TAG, "setChildId");
            if (TextUtils.isEmpty(childId)){
                Log.w(TAG, "remove child id with empty");
                childId = "";
            }

            SharedPreferences.Editor editor =  getSharedPreferences(PREF_NAME,
                    MODE_PRIVATE).edit();
            editor.putString(KEY_CHILD_ID, childId);
            editor.commit();

            if (!TextUtils.isEmpty(childId)){
                mChildId = childId;
                mHandler.sendMessage(mHandler.obtainMessage(MSG_LISTEN_REALTIME_MAPS));
            }
        }
        public String getChildId(){
            String childId;
            childId = getSharedPreferences(PREF_NAME, MODE_PRIVATE)
                    .getString(KEY_CHILD_ID, null);

            if (childId == null)
                Log.w(TAG, "return child id with null");

            Log.w(TAG, "getChildId");

            return childId;
        }

        public Map getCtrlPermissions(){
            return mPublicCtrlPermission;
        }

        public Map getBanTimeMap(){
            return mBanTimeMap;
        }

        public boolean updateBanTimeGame(int index, String what){
            try {
                Log.d(TAG, "updateBanTimeGame++");
                updateArray(index, what);
                Log.d(TAG, "updateBanTimeGame--");
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }
    };

    private final static String KEY_BACKUP_LOCATION_STATE = "backup_location_provider";

    private void enableGpsIfNeeded(){
//        final String locationState = Settings.Secure.getString(mContext.getContentResolver(),
//                LOCATION_PROVIDERS_ALLOWED);
//
//        SharedPreferences.Editor editor = getSharedPreferences(PREF_NAME,
//                MODE_PRIVATE).edit();
//        editor.putString(KEY_BACKUP_LOCATION_STATE, locationState);
//        editor.commit();
        // This will have a dialog showing.
        String addingGPSProvider = "+gps"; //test GPS valid
        Settings.Secure.putString(this.getContentResolver(), LOCATION_PROVIDERS_ALLOWED,
                addingGPSProvider);
        String addingNetworkProvider = "+network"; //test GPS valid
        Settings.Secure.putString(this.getContentResolver(), LOCATION_PROVIDERS_ALLOWED,
                addingNetworkProvider);

    }

    /**
     * Only called by message MSG_ENABLE_GPS
     */
    private void handleCloseGps(){
//        final String savedLocationState = getSharedPreferences(PREF_NAME,
//                MODE_PRIVATE).getString(KEY_BACKUP_LOCATION_STATE, "");
        Log.d(TAG, "handleCloseGps");
        Settings.Secure.putString(mContext.getContentResolver(), LOCATION_PROVIDERS_ALLOWED, "");
    }

    /**
     * MSG_ENABLE_GPS
     */
    private void resettingGpsTimeout(){
        mHandler.removeMessages(MSG_CLOSE_GPS);
        Message closeGpsMessage = mHandler.obtainMessage(MSG_CLOSE_GPS);
        mHandler.sendMessageDelayed(closeGpsMessage, CLOSE_GPS_TIMEOUT);
    }

    private Float getKBs(Long bytes){
        return (float)bytes/1024L;
    }

    // Here the unit is byte
    final static String primaryVolume = "/storage/emulated/0";
    final static String systemPath = "/system/";
    final static String CATEGORY_MUSIC = "music";
    final static String CATEGORY_PHOTO_VIDEO = "video_pic";
    final static String CATEGORY_SYSTEM = "system";
    final static String CATEGORY_AVAILABLE = "available";
    final static String CATEGORY_TOTAL = "total";
    private Bundle measureExactStorage(IMediaContainerService imcs) {
        final Context context = mContext;
        if (context == null) {
            return null;
        }

        HashMap<String, HashMap<String, Long>> categories = new HashMap<>();
        HashMap<String, Long> musicMap = new HashMap<>();

        musicMap.put(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).toString(),0L);
        musicMap.put(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_ALARMS).toString(),0L);
        musicMap.put(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_NOTIFICATIONS).toString(),0L);
        musicMap.put(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_RINGTONES).toString(),0L);
        musicMap.put(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PODCASTS).toString(),0L);

        HashMap<String,Long> videoPhotoMap = new HashMap<>();
        videoPhotoMap.put(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).toString(),0L);
        videoPhotoMap.put(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).toString(),0L);

        HashMap<String,Long> systemMap = new HashMap<>();
        systemMap.put(systemPath , 0L);

        categories.put(CATEGORY_MUSIC, musicMap);
        categories.put(CATEGORY_PHOTO_VIDEO, videoPhotoMap);
        categories.put(CATEGORY_SYSTEM, systemMap);

        Bundle resultMap = new Bundle();
        long volumeAvailable = -1, volumeTotal = -1;
        try {
            long[] sizeArray = imcs.getFileSystemStats(primaryVolume);
            volumeTotal = sizeArray[0];
            volumeAvailable = sizeArray[1];
            Log.d(TAG, "volumeTotal:" + volumeTotal + " volumeAvailable " + volumeAvailable);
        }catch (Exception ex){
            ex.printStackTrace();
        }

        //resultMap.put(CATEGORY_TOTAL, volumeTotal); No need to use this value
        resultMap.putLong(CATEGORY_AVAILABLE, volumeAvailable);

        for (String typeOfCategories : categories.keySet()){
            Log.d(TAG,"Type: " + typeOfCategories + " processing");

            HashMap<String, Long> category = categories.get(typeOfCategories);
            long categorySize = 0;
            for (String path : category.keySet()){
                try {
                    final long size = imcs.calculateDirectorySize(path);
                    Log.d(TAG, "measureExactStorage path" + path + " size:" + size);
                    category.put(path, size);
                    categorySize += size;
                } catch (RemoteException e) {
                    e.printStackTrace();
                } finally{
                    Log.d(TAG, "measureExactStorage finally" + categorySize);
                    resultMap.putLong(typeOfCategories, categorySize);
                }
            }
        }

        for (String k : resultMap.keySet())
            Log.d(TAG, "result type:\t" + k + '\t' + resultMap.get(k));

        return resultMap;
    }

}
