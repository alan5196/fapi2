package com.fennec.apicallexample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class FennecBootReceiver extends BroadcastReceiver {
	private static final String LOG_TAG = "FennecBootReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		Log.d(LOG_TAG, "onReceive " + intent);

		if(action!=null && action.equals(Intent.ACTION_BOOT_COMPLETED)){
			Log.d(LOG_TAG, "Receive boot complete, start FennecDispatchingService");
			Intent service;
			try {
				service = new Intent(context, DispatchingService.class);
				context.startService(service);
			} catch (Exception e) {
				Log.e(LOG_TAG, "Can't start service."+e);
			}
		}
	}
}
