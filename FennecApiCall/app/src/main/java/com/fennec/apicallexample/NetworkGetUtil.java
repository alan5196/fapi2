package com.fennec.apicallexample;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.ANResponse;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.Map;

class NetworkGetUtil extends NetworkUtil {
    private Map<String, String> mQueryParams;
    private Map<String, String> mHeaders;

    NetworkGetUtil(@Nullable Map<String,String> headers,@Nullable Map<String,String> queryParams, @NonNull String... apis){
        mQueryURL = buildRequestURL(apis);
        mQueryParams = queryParams;
        mHeaders = headers;
    }

    final TaskCompletionSource<JSONObject> taskCompletionSource = new TaskCompletionSource<>();
    class MyAsyncTask extends AsyncTask<TaskCompletionSource<JSONObject>, Void, TaskCompletionSource<JSONObject>> {
        TaskCompletionSource<JSONObject> mResult;
        @Override
        protected TaskCompletionSource<JSONObject> doInBackground(TaskCompletionSource<JSONObject>... taskCompletionSources) {

            if (taskCompletionSources == null){
                return null;
            }

            mResult = taskCompletionSources[0];
            Log.d(TAG, "MyAsyncTask doInBackground");

            ANRequest request = AndroidNetworking.get(mQueryURL)
                    .addQueryParameter(mQueryParams)
                    .addHeaders(mHeaders)
                    .build();
            ANResponse<JSONObject> response = request.executeForJSONObject();

            if (response.isSuccess()){
                try {
                    JSONObject jsonObject = response.getResult();
                    Log.d(TAG, "MyAsyncTask doInBackground" + jsonObject.toString());
                    if (jsonObject.getBoolean("success")){
                        mResult.setResult(jsonObject.getJSONObject("user"));
                    } else{
                        mResult.setResult(new JSONObject());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    mResult.setException(e);
                }
            } else {
                Log.e(TAG,"Error: " + response.getError().getErrorBody());
                mResult.setException(new Exception("Not ok response is not success"));
            }

            return mResult;
        }

    }

    public Task<JSONObject> startWork() {
        MyAsyncTask task = new MyAsyncTask();
        task.execute(taskCompletionSource);
        return taskCompletionSource.getTask();
    }

}