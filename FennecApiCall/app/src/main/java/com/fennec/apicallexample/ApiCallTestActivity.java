package com.fennec.apicallexample;

import android.animation.ArgbEvaluator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;

import butterknife.ButterKnife;
import butterknife.BindView;
import butterknife.OnClick;

public class ApiCallTestActivity extends AppCompatActivity{

    private String TAG = this.getClass().getSimpleName();

    @BindView(R.id.button_1_1_2) Button button_1_1_2;
    @BindView(R.id.button_1_1_3) Button button_1_1_3;

    @OnClick({R.id.button_1_1_2,R.id.button_1_1_3})
    public void submit(View view) {
        // TODO submit data to server...
        Toast.makeText(this, "Make test", Toast.LENGTH_LONG).show();

    }


    private static String SERVER_IP = "35.201.195.142:1337/api/";
    private void postMethod(String... apis){
        if (apis == null)
            return;

        StringBuilder requestApiString = new StringBuilder();

        for (String api : apis)
            requestApiString.append(api).append('/');

//        AndroidNetworking.post(requestApiString.toString()).addJSONObjectBody();
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.api_call_test_layout);
        ButterKnife.bind(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


}
