package com.fennec.apicallexample;

import android.util.Log;
import org.json.JSONObject;

abstract class NetworkUtil  {
    protected final String TAG = this.getClass().getSimpleName();
    protected String mQueryURL = "http://35.201.195.142:1337/api/";

    protected String buildRequestURL(String... apis){
        if (apis == null)
            return mQueryURL;

        StringBuilder urlBuilder = new StringBuilder(mQueryURL);
        for (String str : apis)
            urlBuilder.append(str).append('/');
        String result = urlBuilder.toString();

        Log.d(TAG, "mQueryURL = " + result);
        return result;
    }
}
