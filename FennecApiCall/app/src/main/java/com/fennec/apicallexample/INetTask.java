package com.fennec.apicallexample;

import com.google.android.gms.tasks.Task;

import org.json.JSONObject;

public interface INetTask<T> {
    Task<T> startWork();
}
